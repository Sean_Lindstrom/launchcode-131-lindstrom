package studio1;

import cse131.ArgsProcessor;

/**
 * From Sedgewick and Wayne, COS 126 course at Princeton
 * 
 */
public class HiFour {
	public static void main(String[] args) {
		ArgsProcessor ap = new ArgsProcessor(args);
		String name1 = ap.nextString("Enter your name.");
		String name2 = ap.nextString("Enter another name.");
		String name3 = ap.nextString("Enter yet another name.");
		String name4 = ap.nextString("Enter, for the last time, a name.");
		//
		// Say hello to the names in s0 through s3.
		//
		System.out.println("Hello, " + name1 + ", " + name2 + ", " + name3 + ", " + name4 + "!");

	}
}

